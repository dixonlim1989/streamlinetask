/* Global Styling Constants */

import { StyleSheet, Dimensions } from "react-native";

const SCREEN_WIDTH = Dimensions.get("window").width;

export const styles = StyleSheet.create({
  flexOne: {
    flex: 1,
    backgroundColor: "#ffffff",
  },
  container: {
    flex: 1,
    padding: 15,
    backgroundColor: "#ffffff",
  },
  homeWeatherContainer: {
    alignItems: "center",
    justifyContent: "center",
  },
  imgIcon: {
    width: "50%",
    height: "25%",
  },
  imgIconList: {
    width: 50,
    height: 50,
  },
  menuHeader: {
    fontWeight: "700",
    fontSize: 25,
  },
  celciusTxt: {
    fontWeight: "500",
    fontSize: 15,
  },
  listContainer: {
    paddingVertical: 20,
  },
  lists: {
    paddingVertical: 10,
    justifyContent: "center",
  },
  headerTxt: {
    fontWeight: "500",
    fontSize: 20,
  },
  center: {
    justifyContent: "center",
    alignItems: "center",
  },
  row: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  touchableContainer: { paddingVertical: 10, paddingHorizontal: 15, borderWidth: 1 },
  vwRightIconContainer: {
    width: SCREEN_WIDTH / 6,
    justifyContent: "flex-end",
    alignItems: "center",
    flexDirection: "row",
  },
  vwLeftIconContainer: {
    width: SCREEN_WIDTH / 6,
  },
  vwContainer: {
    height: 40,
    width: SCREEN_WIDTH,
    flexDirection: "row",
  },
  vwLogoContainer: {
    paddingLeft: 6,
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  btnLeftNav: {
    paddingLeft: 10,
    flex: 1,
    justifyContent: "center",
    alignItems: "flex-start",
  },
  btnRightNav: {
    paddingRight: 16,
    justifyContent: "center",
    flex: 1,
    alignItems: "flex-end",
  },
});

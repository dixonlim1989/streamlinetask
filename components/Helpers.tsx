/* Helpers functionality for Location Permissions */

import * as Permissions from "expo-permissions";
import * as Location from "expo-location";

class Helpers {
  askLocationsPermissionStatus = async () => {
    const { status } = await Permissions.askAsync(Permissions.LOCATION);
    return status;
  };

  locationsPermissionStatus = async () => {
    const { status } = await Permissions.getAsync(Permissions.LOCATION);
    return status;
  };

  getCurrentPosition = async () => {
    let location = await Location.getCurrentPositionAsync({
      accuracy: Location.Accuracy.Highest,
    });
    return location;
  };
}

export default new Helpers();

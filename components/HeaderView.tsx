/* Header Component */

import React from "react";
import { View, TouchableOpacity, Dimensions } from "react-native";
const SCREEN_WIDTH = Dimensions.get("window").width;
import { styles } from "../constants/globalStyles";

const HeaderNavCustView = ({
  navBackgroundColor = "#ffffff",
  leftIcon = null,
  leftNav = null,
  rightNav,
  rightIcon = null,
  rightNavDisabled = false,
  logo = null,
  topMargin = 0,
  navCustHeight = 40,
  rightNavWidth = SCREEN_WIDTH / 6,
  leftNavWidth = SCREEN_WIDTH / 6,
  leftIconJustify,
  zIndex,
  propStyle,
}) => {
  return (
    <View
      pointerEvents="box-none"
      style={[
        styles.vwContainer,
        {
          marginTop: topMargin,
          backgroundColor: navBackgroundColor,
          height: navCustHeight,
          zIndex: zIndex,
        },
      ]}
    >
      <View style={[styles.vwLeftIconContainer, { width: leftNavWidth }]}>
        {leftIcon && (
          <TouchableOpacity
            onPress={leftNav}
            style={[styles.btnLeftNav, { width: leftNavWidth, justifyContent: leftIconJustify }]}
          >
            {leftIcon}
          </TouchableOpacity>
        )}
      </View>

      <View style={styles.vwLogoContainer}>{logo}</View>

      <View style={[styles.vwRightIconContainer, { width: rightNavWidth }, { ...propStyle }]}>
        {rightIcon && (
          <TouchableOpacity
            disabled={rightNavDisabled}
            onPress={rightNav}
            style={[styles.btnRightNav, { width: rightNavWidth, justifyContent: leftIconJustify }, { ...propStyle }]}
          >
            {rightIcon}
          </TouchableOpacity>
        )}
      </View>
    </View>
  );
};

export default HeaderNavCustView;


/* Location Page 
  * Mainly getting data from weather API.
  * Once loaded the list of 7 days forcast will be displayed in Location page based on Location Name.
  * Due to limited access to the API I was not able to fully utilise the API itself.
  * I was hoping this will be enough to demonstrate my React Native coding skills.
*/

import * as React from "react";
import { Text, View, SafeAreaView, Image, ActivityIndicator, ScrollView } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome5";
import { styles } from "../constants/globalStyles";
import HeaderView from "../components/HeaderView";
import { findWeatherViaNext7Days } from "../api/weather_api";

interface PropsInterface {
  navigation: any;
}

interface StateInterface {
  item: any;
  next7DaysList: any;
  isLoading: boolean;
}

class Location extends React.Component<PropsInterface, StateInterface> {
  public constructor(props: PropsInterface) {
    super(props);
    this.state = {
      item: this.props.navigation.getParam("item"),
      next7DaysList: [],
      isLoading: false,
    };
  }

  public componentDidMount = async () => {
    const { item } = this.state;
    this.setState({ isLoading: true });
    let next7DaysList = await this.getWeatherByNext7Days(item.location.name);
    this.setState({ next7DaysList });
    this.setState({ isLoading: false });
  };

  private getWeatherByNext7Days = async (item: string) => {
    try {
      let request = await findWeatherViaNext7Days(item);
      if (request.status === 200) {
        return request.data;
      }
    } catch (e) {
      console.log(e);
    }
  };

  private navigateToBack = () => {
    this.props.navigation.goBack();
  };

  public render(): React.ReactNode {
    const { item, isLoading, next7DaysList } = this.state;

    return (
      <SafeAreaView style={styles.flexOne}>
        <HeaderView
          logo={
            <View>
              <Text style={styles.menuHeader}>{item.location.name}</Text>
            </View>
          }
          leftIcon={<Icon name="chevron-left" color={"#000000"} size={20} />}
          leftNav={() => this.navigateToBack()}
        />
        <View style={styles.container}>
          <View style={styles.homeWeatherContainer}>
            <Text style={styles.headerTxt}>{"Region: " + item.location.region + " Weather"}</Text>
            <Image
              style={styles.imgIcon}
              source={{ uri: "http:" + item.current.condition.icon }}
              resizeMode={"contain"}
            />
            <Text style={styles.celciusTxt}>{item.current.feelslike_c + "°C"}</Text>
            <Text style={styles.celciusTxt}>{item.current.condition.text}</Text>
          </View>
          <ScrollView>
            <View>
              <Text style={styles.headerTxt}>List of Next 7 Days</Text>
              <View>
                {!isLoading && next7DaysList.forecast && next7DaysList.forecast.forecastday ? (
                  <View>
                    {next7DaysList.forecast.forecastday.map((item: any, i: number) => (
                      <View style={styles.lists} key={i}>
                        <View style={styles.row}>
                          <View style={styles.center}>
                            <Image
                              style={styles.imgIconList}
                              source={{ uri: "http:" + item.day.condition.icon }}
                              resizeMode={"contain"}
                            />
                            <Text>{item.day.condition.text}</Text>
                          </View>
                          <View style={styles.center}>
                            <Text style={styles.headerTxt}>{item.date}</Text>
                          </View>
                          <View style={styles.center}>
                            <Text style={styles.celciusTxt}>{item.day.avgtemp_c + "°C"}</Text>
                          </View>
                        </View>
                      </View>
                    ))}
                  </View>
                ) : (
                  <View style={[styles.center, { flex: 1 }]}>
                    <ActivityIndicator size="large" color="#000000" />
                  </View>
                )}
              </View>
            </View>
          </ScrollView>
        </View>
      </SafeAreaView>
    );
  }
}

export default Location;

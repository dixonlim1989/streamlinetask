/* Home Page
 * Mainly getting data from weather API.
 * I have added a location services to grab your current location weather data.
 * If location is enabled it will not show the Location Icon once loaded.
 * I am grabbing list of 10 Major cities for viewing purposes.
 * You will be able to touch the list of cities and navigate to the Location Page.
 */

import * as React from "react";
import { Text, View, SafeAreaView, Image, ActivityIndicator, TouchableOpacity, ScrollView } from "react-native";
import HeaderView from "../components/HeaderView";
import Icon from "react-native-vector-icons/FontAwesome5";
import Helpers from "../components/Helpers";
import { findWeatherViaCity, findWeatherViaLocation } from "../api/weather_api";
import { styles } from "../constants/globalStyles";
import { WEATHER_API_KEY } from "../constants/constants";

interface PropsInterface {
  navigation: any;
}

interface StateInterface {
  homeWeather: any;
  majorCities: any;
  isLocationEnabled: boolean;
  isLoading: boolean;
  cityList: any;
}

const defaultCity = "Melbourne";

class Home extends React.Component<PropsInterface, StateInterface> {
  public constructor(props: PropsInterface) {
    super(props);
    this.state = {
      homeWeather: [],
      majorCities: ["Beijing", "Chicago", "Cairo", "Tokyo", "Mumbai", "Delhi", "London", "Jakarta", "Paris", "Manila"],
      isLocationEnabled: false,
      isLoading: false,
      cityList: [],
    };
  }

  public componentDidMount = async () => {
    const { majorCities } = this.state;
    const status = await Helpers.locationsPermissionStatus();

    this.setState({ isLoading: true });

    if (status === "denied" || "undetermined") {
      this.setState({ isLocationEnabled: false });
      let homeWeather = await this.getWeatherByCity(defaultCity);
      this.setState({ homeWeather });
    }
    if (status === "granted") {
      this.setState({ isLocationEnabled: true });
      let location = await this.getLocationAsync();
      const { latitude, longitude } = location.coords;
      let homeWeather = await this.getWeatherByLocation(latitude, longitude);
      this.setState({ homeWeather });
    }

    let cityList = [];
    for (let i = 0; i < majorCities.length; i++) {
      let cityWeather = await this.getWeatherByCity(majorCities[i]);
      cityList.push(cityWeather);
    }

    this.setState({ cityList, isLoading: false });
  };

  private getWeatherByCity = async (city: string) => {
    try {
      let request = await findWeatherViaCity(city);
      if (request.status === 200) {
        return request.data;
      }
    } catch (e) {
      console.log(e);
    }
  };

  private getWeatherByLocation = async (lat: any, lng: any) => {
    try {
      let request = await findWeatherViaLocation(lat, lng);
      if (request.status === 200) {
        return request.data;
      }
    } catch (e) {
      console.log(e);
    }
  };

  private enableLocationServices = async () => {
    let status = await Helpers.askLocationsPermissionStatus();
    if (status !== "granted") {
      this.setState({ isLocationEnabled: false });
      console.log("Status Not Enabled");
    }
    this.getLocationAsync();
  };

  private getLocationAsync = async () => {
    const status = await Helpers.locationsPermissionStatus();
    try {
      if (status !== "granted") {
        this.setState({ isLocationEnabled: false });
        console.log("Status Not Enabled");
      }
      let location = await Helpers.getCurrentPosition();
      return location;
    } catch (e) {
      console.log(e);
    }
  };

  private navigateToLocationPage = (item: any) => {
    this.props.navigation.navigate("Location", { item });
  };

  public render(): React.ReactNode {
    const { homeWeather, isLoading, isLocationEnabled, cityList } = this.state;
    return (
      <SafeAreaView style={styles.flexOne}>
        <HeaderView
          logo={
            <View>
              <Text style={styles.menuHeader}>Weather App</Text>
            </View>
          }
          rightIcon={!isLocationEnabled ? <Icon name="location-arrow" color={"#000000"} size={20} /> : null}
          rightNav={() => this.enableLocationServices()}
        />
        {WEATHER_API_KEY !== "" ? (
          <View style={styles.container}>
            {!isLoading && homeWeather.current && cityList ? (
              <React.Fragment>
                <View style={styles.homeWeatherContainer}>
                  <Text style={styles.headerTxt}>
                    {homeWeather.location.name + ", " + homeWeather.location.region + " Weather"}
                  </Text>
                  <Image
                    style={styles.imgIcon}
                    source={{ uri: "http:" + homeWeather.current.condition.icon }}
                    resizeMode={"contain"}
                  />
                  <Text style={styles.celciusTxt}>{homeWeather.current.feelslike_c + "°C"}</Text>
                  <Text style={styles.celciusTxt}>{homeWeather.current.condition.text}</Text>
                </View>
                <View>
                  <Text style={styles.headerTxt}>List of Major Cities in the World</Text>
                </View>
                <ScrollView style={styles.listContainer} showsVerticalScrollIndicator={false}>
                  {cityList.map((item: any, i: number) => (
                    <View style={styles.lists} key={i}>
                      <TouchableOpacity
                        onPress={() => this.navigateToLocationPage(item)}
                        style={styles.touchableContainer}
                      >
                        <View style={styles.row}>
                          <View style={styles.center}>
                            <Image
                              style={styles.imgIconList}
                              source={{ uri: "http:" + item.current.condition.icon }}
                              resizeMode={"contain"}
                            />
                            <Text>{item.current.condition.text}</Text>
                          </View>
                          <View style={styles.center}>
                            <Text style={styles.headerTxt}>{item.location.name}</Text>
                          </View>
                          <View style={styles.center}>
                            <Text style={styles.celciusTxt}>{item.current.feelslike_c + "°C"}</Text>
                          </View>
                        </View>
                      </TouchableOpacity>
                    </View>
                  ))}
                </ScrollView>
              </React.Fragment>
            ) : (
              <View style={[styles.center, { flex: 1 }]}>
                <ActivityIndicator size="large" color="#000000" />
              </View>
            )}
          </View>
        ) : (
          <View style={[styles.center, { flex: 1 }]}>
            <Text style={[styles.menuHeader, { textAlign: "center" }]}>
              Please Provide Your OWN API KEY for the weather API to work
            </Text>
          </View>
        )}
      </SafeAreaView>
    );
  }
}

export default Home;

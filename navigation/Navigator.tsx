/* Navigator -> Making sure pages are able to navigate between stacks */

import Home from "../screen/Home";
import Location from "../screen/Location";
import { createStackNavigator } from "react-navigation-stack";
import { createAppContainer } from "react-navigation";

const Navigators = createStackNavigator(
  {
    Home: {
      screen: Home
    },
    Location: {
      screen: Location
    },
  },
  {
    headerMode: "none",
    navigationOptions: {
      gesturesEnabled: false
    }
  }
);

const Navigator = createAppContainer(Navigators);

export default Navigator;
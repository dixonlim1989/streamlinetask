/* Weather API */

import axios from "axios";
import { WEATHER_API_KEY } from "../constants/constants";

const WEATHER_API_URL = "http://api.weatherapi.com/v1/current.json";
const FORECAST_API_URL = "http://api.weatherapi.com/v1/forecast.json";

export function findWeatherViaCity(city: string) {
  return axios.get(`${WEATHER_API_URL}?key=${WEATHER_API_KEY}&q=${city}`);
}

export function findWeatherViaLocation(lat: any, lng: any) {
  return axios.get(`${WEATHER_API_URL}?key=${WEATHER_API_KEY}&q=${lat},${lng}`);
}

export function findWeatherViaNext7Days(city: string) {
  return axios.get(`${FORECAST_API_URL}?key=${WEATHER_API_KEY}&q=${city}&days=7`);
}
